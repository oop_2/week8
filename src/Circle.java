public class Circle {
    private double radius;
    private Double area;
    private double per;
    public Circle(double d) {
        this.radius = d;
    }
    public Double printCircleArea() {
        area = (Double) (3.14 * radius * radius);
        System.out.println("Area of Circle = "+ area);
        return area;
    }
    public Double printCirclePerimeter() {
        per = 2 * 3.14 * radius;
        System.out.println("Perimeter of Circle = "+ per);
        return per;
    }
}