public class Rectangle {
    private int height;
    private int width;
    private int area;
    private int per;
    public Rectangle(int height, int width) {
        this.height = height;
        this.width = width;
    }
    public int printRectangleArea1() {
        area = height * width;
        System.out.println("Area of rectangle1 = "+ area);
        return area;
    }
    public int printRectangleArea2() {
        area = height * width;
        System.out.println("Area of rectangle2 = "+ area);
        return area;
    }
    public int printRectanglePerimeter1() {
        per = (height * 2) + (width * 2);
        System.out.println("Perimeter of rectangle1 = "+per);
        return per;
    }
    public int printRectanglePerimeter2() {
        per = (height * 2) + (width * 2);
        System.out.println("Perimeter of rectangle2 = "+per);
        return per;
    }
}
